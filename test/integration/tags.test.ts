import * as supertest from 'supertest';
import * as uuid      from 'uuid';
import { expect }     from 'chai';
import { eachSeries } from 'async';

const api = supertest('http://localhost:3000');

describe('/v1/tags', function () {
  describe('when GET', function () {
    describe('and no items are yet added', function () {
      it('should return an empty collection', function (done) {
        api
          .get(`/v1/tags`)
          .set('Content-Type', 'application/vnd.api+json')
          .expect(function (res) {
            const body = JSON.parse(res.text);

            expect(body).to.have.property('data')
                        .and.have.lengthOf(0);
          })
          .expect(200, done);
      });
    });

    describe('and page[size]=2 and total=3', function () {
      before(function (done) {
        this.tags = [
          {
            name: `tag-${uuid.v4()}`
          },
          {
            name: `tag-${uuid.v4()}`
          },
          {
            name: `tag-${uuid.v4()}`
          }
        ];

        eachSeries(this.tags, function (item: any, cb) {
          let clientGeneratedId: string = `client-generated-id-${uuid.v4()}`;

          api.post('/v1/tags')
             .set('Content-Type', 'application/vnd.api+json')
             .send(JSON.stringify({
               data: {
                 id        : clientGeneratedId,
                 type      : 'tag',
                 attributes: item
               }
             }))
             .expect(function (res) {
               const body = JSON.parse(res.text);
               item.id    = body.data.id;
             })
             .expect(201, cb);
        }, done);
      });

      after(function (done) {
        eachSeries(this.tags, function (item: any, cb) {
          api.delete('/v1/tags/' + item.id)
             .expect(204, cb);
        }, done);
      });

      it('should return the list reflecting the proper pagination object', function (done) {
        api
          .get(`/v1/tags`)
          .set('Content-Type', 'application/vnd.api+json')
          .query({
            page: {
              size: 2
            }
          })
          .expect(function (res) {
            const body = JSON.parse(res.text);

            expect(body).to.have.property('links')
                        .and.have
                        .keys([
                          'next'
                        ]);

            expect(body).to.have.property('data')
                        .and.have.lengthOf(2);
          })
          .expect(200, done);
      });
    });
  });

  describe('when POST', function () {
    describe('and creating a valid tag', function () {
      it('should create a tag and return a valid JSON API response', function (done) {
        let self                      = this;
        let clientGeneratedId: string = `client-generated-id-${uuid.v4()}`;

        after(function (done) {
          api.delete('/v1/tags/' + self.tagId)
             .expect(204, done);
        });

        api
          .post('/v1/tags')
          .set('Content-Type', 'application/vnd.api+json')
          .send(JSON.stringify({
            data: {
              id        : clientGeneratedId,
              type      : 'tag',
              attributes: {
                name: `tag-${uuid.v4()}`
              }
            }
          }))
          .expect(function (res) {
            const body = JSON.parse(res.text);

            expect(body).to.have.property('data')
                        .and.have
                        .keys([
                          'id',
                          'type',
                          'attributes',
                          'links'
                        ]);

            expect(body.data.id).eq(clientGeneratedId);
            self.tagId = body.data.id;

            expect(body.data.attributes).to.have.keys([
              'name',
              'createdAt',
              'updatedAt'
            ]);
          })
          .expect(201, done);
      });
    });
  });

  describe('/v1/tags/{tagId}', function () {
    describe('when GET ', function () {
      describe('a valid tag', function () {
        before(function (done) {
          let self = this;

          api.post('/v1/tags')
             .set('Content-Type', 'application/vnd.api+json')
             .send(JSON.stringify({
               data: {
                 id        : `client-generated-id-${uuid.v4()}`,
                 type      : 'tag',
                 attributes: {
                   name: `tag-${uuid.v4()}`
                 }
               }
             }))
             .expect(function (res) {
               const body = JSON.parse(res.text);
               self.tagId = body.data.id;
             })
             .expect(201, done);
        });

        after(function (done) {
          api.delete('/v1/tags/' + this.tagId)
             .expect(204, done);
        });

        it('should return a valid tag response', function (done) {
          let self = this;

          api
            .get(`/v1/tags/${self.tagId}`)
            .set('Content-Type', 'application/vnd.api+json')
            .expect(function (res) {
              const body = JSON.parse(res.text);

              expect(body).to.have.property('data')
                          .and.have
                          .keys([
                            'id',
                            'type',
                            'attributes',
                            'links'
                          ]);

              expect(body.data.id).to.be.eq(self.tagId);

              expect(body.data.attributes).to.have.keys([
                'name',
                'createdAt',
                'updatedAt'
              ]);
            })
            .expect(200, done);
        });
      });
    });

    describe('when DELETE ', function () {
      describe('a valid tag', function () {
        before(function (done) {
          let self = this;

          api.post('/v1/tags')
             .set('Content-Type', 'application/vnd.api+json')
             .send(JSON.stringify({
               data: {
                 id        : `client-generated-id-${uuid.v4()}`,
                 type      : 'tag',
                 attributes: {
                   name: `tag-${uuid.v4()}`
                 }
               }
             }))
             .expect(function (res) {
               const body         = JSON.parse(res.text);
               self.tagToDeleteId = body.data.id;
             })
             .expect(201, done);
        });

        it('should return a valid response', function (done) {
          api.delete('/v1/tags/' + this.tagToDeleteId)
             .expect(204, done);
        });
      });
    });

    describe('when PATCH', function () {
      describe('a valid tag', function () {
        describe('with valid update values', function () {
          before(function (done) {
            let self = this;

            api.post('/v1/tags')
               .set('Content-Type', 'application/vnd.api+json')
               .send(JSON.stringify({
                 data: {
                   id        : `client-generated-id-${uuid.v4()}`,
                   type      : 'tag',
                   attributes: {
                     name: `tag-${uuid.v4()}`
                   }
                 }
               }))
               .expect(function (res) {
                 const body = JSON.parse(res.text);
                 self.tagId = body.data.id;
               })
               .expect(201, done);
          });

          after(function (done) {
            api.delete('/v1/tags/' + this.tagId)
               .expect(204, done);
          });

          it('should update the values and return the updated data', function (done) {
            let self = this;

            self.someOtherName = `some-other-name-${uuid.v4()}`;

            api
              .patch('/v1/tags/' + self.tagId)
              .set('Content-Type', 'application/vnd.api+json')
              .send(JSON.stringify({
                data: {
                  type      : 'tag',
                  id        : self.tagId,
                  attributes: {
                    name: self.someOtherName
                  }
                }
              }))
              .expect(function (res) {
                const body = JSON.parse(res.text);

                expect(body).to.have.property('data')
                            .and.have
                            .keys([
                              'id',
                              'type',
                              'attributes',
                              'links'
                            ]);

                expect(body.data.id).to.be.eq(self.tagId);

                expect(body.data.attributes).to.have.keys([
                  'name',
                  'createdAt',
                  'updatedAt'
                ]);

                expect(body.data.attributes.name).to.be.eq(self.someOtherName);
              })
              .expect(200, done);
          });
        });
      });
    });
  });
});
