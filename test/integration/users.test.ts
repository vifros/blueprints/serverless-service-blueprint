import * as supertest from 'supertest';
import * as uuid      from 'uuid';
import { expect }     from 'chai';
import { eachSeries } from 'async';

const api = supertest('http://localhost:3000');

describe('/v1/users', function () {
  describe('when GET', function () {
    describe('and no items are yet added', function () {
      it('should return an empty collection', function (done) {
        api
          .get(`/v1/users`)
          .set('Content-Type', 'application/vnd.api+json')
          .expect(function (res) {
            const body = JSON.parse(res.text);

            expect(body).to.have.property('data')
                        .and.have.lengthOf(0);
          })
          .expect(200, done);
      });
    });

    describe('and page[size]=2 and total=3', function () {
      before(function (done) {
        this.users = [
          {
            username: `user-${uuid.v4()}`
          },
          {
            username: `user-${uuid.v4()}`
          },
          {
            username: `user-${uuid.v4()}`
          }
        ];

        eachSeries(this.users, function (item: any, cb) {
          api.post('/v1/users')
             .set('Content-Type', 'application/vnd.api+json')
             .send(JSON.stringify({
               data: {
                 type      : 'user',
                 attributes: item
               }
             }))
             .expect(function (res) {
               const body = JSON.parse(res.text);
               item.id    = body.data.id;
             })
             .expect(201, cb);
        }, done);
      });

      after(function (done) {
        eachSeries(this.users, function (item: any, cb) {
          api.delete('/v1/users/' + item.id)
             .expect(204, cb);
        }, done);
      });

      it('should return the list reflecting the proper pagination object', function (done) {
        api
          .get(`/v1/users`)
          .set('Content-Type', 'application/vnd.api+json')
          .query({
            page: {
              size: 2
            }
          })
          .expect(function (res) {
            const body = JSON.parse(res.text);

            expect(body).to.have.property('links')
                        .and.have
                        .keys([
                          'next'
                        ]);

            expect(body).to.have.property('data')
                        .and.have.lengthOf(2);
          })
          .expect(200, done);
      });
    });
  });

  describe('when POST', function () {
    describe('and creating a valid user', function () {
      it('should create a user and return a valid JSON API response', function (done) {
        let self = this;

        after(function (done) {
          api.delete('/v1/users/' + self.userId)
             .expect(204, done);
        });

        api
          .post('/v1/users')
          .set('Content-Type', 'application/vnd.api+json')
          .send(JSON.stringify({
            data: {
              type      : 'user',
              attributes: {
                username: `user-${uuid.v4()}`
              }
            }
          }))
          .expect(function (res) {
            const body = JSON.parse(res.text);

            expect(body).to.have.property('data')
                        .and.have
                        .keys([
                          'id',
                          'type',
                          'attributes',
                          'links'
                        ]);

            self.userId = body.data.id;

            expect(body.data.attributes).to.have.keys([
              'isAdmin',
              'username',
              'createdAt',
              'updatedAt'
            ]);
          })
          .expect(201, done);
      });
    });
  });

  describe('/v1/users/{userId}', function () {
    describe('when GET ', function () {
      describe('a valid user', function () {
        before(function (done) {
          let self = this;

          api.post('/v1/users')
             .set('Content-Type', 'application/vnd.api+json')
             .send(JSON.stringify({
               data: {
                 type      : 'user',
                 attributes: {
                   isAdmin : true,
                   username: `user-${uuid.v4()}`,
                   address : {
                     streetAddress: 'Street 41st',
                     state        : 'Some State',
                     country      : 'XU',
                     postalCode   : 99999
                   }
                 }
               }
             }))
             .expect(function (res) {
               const body  = JSON.parse(res.text);
               self.userId = body.data.id;
             })
             .expect(201, done);
        });

        after(function (done) {
          api.delete('/v1/users/' + this.userId)
             .expect(204, done);
        });

        it('should return a valid user response', function (done) {
          let self = this;

          api
            .get(`/v1/users/${self.userId}`)
            .set('Content-Type', 'application/vnd.api+json')
            .expect(function (res) {
              const body = JSON.parse(res.text);

              expect(body).to.have.property('data')
                          .and.have
                          .keys([
                            'id',
                            'type',
                            'attributes',
                            'links'
                          ]);

              expect(body.data.id).to.be.eq(self.userId);

              expect(body.data.attributes).to.have.keys([
                'isAdmin',
                'username',
                'address',
                'createdAt',
                'updatedAt'
              ]);

              expect(body.data.attributes.isAdmin).to.be.true;
            })
            .expect(200, done);
        });
      });
    });

    describe('when DELETE ', function () {
      describe('a valid user', function () {
        before(function (done) {
          let self = this;

          api.post('/v1/users')
             .set('Content-Type', 'application/vnd.api+json')
             .send(JSON.stringify({
               data: {
                 type      : 'user',
                 attributes: {
                   username: `user-${uuid.v4()}`
                 }
               }
             }))
             .expect(function (res) {
               const body          = JSON.parse(res.text);
               self.userToDeleteId = body.data.id;
             })
             .expect(201, done);
        });

        it('should return a valid response', function (done) {
          api.delete('/v1/users/' + this.userToDeleteId)
             .expect(204, done);
        });
      });
    });

    describe('when PATCH', function () {
      describe('a valid user', function () {
        describe('with valid update values', function () {
          before(function (done) {
            let self = this;

            self.address = {
              streetAddress: 'Street 41st',
              state        : 'Some State',
              country      : 'XU',
              postalCode   : 99999
            };

            api.post('/v1/users')
               .set('Content-Type', 'application/vnd.api+json')
               .send(JSON.stringify({
                 data: {
                   type      : 'user',
                   attributes: {
                     username: `user-${uuid.v4()}`,
                     address : self.address
                   }
                 }
               }))
               .expect(function (res) {
                 const body  = JSON.parse(res.text);
                 self.userId = body.data.id;
               })
               .expect(201, done);
          });

          after(function (done) {
            api.delete('/v1/users/' + this.userId)
               .expect(204, done);
          });

          it('should update the values and return the updated data', function (done) {
            let self = this;

            self.someOtherUsername      = `some-other-username-${uuid.v4()}`;
            self.someOtherStreetAddress = 'Street 57st';

            api
              .patch('/v1/users/' + self.userId)
              .set('Content-Type', 'application/vnd.api+json')
              .send(JSON.stringify({
                data: {
                  type      : 'user',
                  id        : self.userId,
                  attributes: {
                    username: self.someOtherUsername,
                    address : {
                      streetAddress: self.someOtherStreetAddress
                    }
                  }
                }
              }))
              .expect(function (res) {
                const body = JSON.parse(res.text);

                expect(body).to.have.property('data')
                            .and.have
                            .keys([
                              'id',
                              'type',
                              'attributes',
                              'links'
                            ]);

                expect(body.data.id).to.be.eq(self.userId);

                expect(body.data.attributes).to.have.keys([
                  'isAdmin',
                  'username',
                  'address',
                  'createdAt',
                  'updatedAt'
                ]);

                expect(body.data.attributes.username).to.be.eq(self.someOtherUsername);

                let newAddress = Object.assign({}, self.address, {
                  streetAddress: self.someOtherStreetAddress
                });

                expect(body.data.attributes.address).to.be.eql(newAddress);
              })
              .expect(200, done);
          });
        });
      });
    });
  });
});
