import {
  defaultThroughput,
  IDbModelLoaderMap
} from '@vifros/serverless-json-api';

export const tagModelLoader: IDbModelLoaderMap = {
  id               : 'tag',
  tableName        : process.env.SERVICE_DYNAMODB_TABLE_TAGS,
  schema           : {
    id       : {
      type    : String,
      hashKey : true,
      readonly: true
    },
    type     : {
      type          : String,
      readonly      : true,
      default       : 'tag',
      enforceDefault: true,
      index         : [
        {
          global    : true,
          name      : 'typeCreatedAtGlobalIndex',
          rangeKey  : 'createdAt',
          project   : true,
          throughput: defaultThroughput
        },
        {
          global    : true,
          name      : 'typeNameGlobalIndex',
          rangeKey  : 'name',
          project   : true,
          throughput: defaultThroughput
        }
      ]
    },
    name     : {
      type    : String,
      required: true
    },
    createdAt: {
      type: Number
    },
    updatedAt: {
      type: Number
    }
  },
  schemaOptions    : {
    throughput: defaultThroughput
  },
  serializerOptions: {
    links: {
      self: function (data) {
        return '/v1/tags/' + data.id;
      }
    }
  },
  metaOptions      : {
    allowClientGeneratedIds: true
  }
};
